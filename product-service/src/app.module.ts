import { Module } from '@nestjs/common'
import { ProductsModule } from './products/products.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Product } from './typeorm/entities/Products'

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'user_db_svc',
            port: 5432,
            database: 'productdb',
            entities: [Product],
            synchronize: true,
            username: 'admin',
            password: 'abc123'
        }),
        ProductsModule
    ],
    controllers: [],
    providers: []
})
export class AppModule {}
