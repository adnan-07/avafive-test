import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'products' })
export class Product {
    @PrimaryGeneratedColumn('uuid')
    productid: string

    @Column({ nullable: false })
    productname: string

    @Column({ nullable: false, type: 'float' })
    price: number

    @Column({ nullable: false, select: false })
    userid: string
}
