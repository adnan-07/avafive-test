import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Product } from 'src/typeorm/entities/Products'
import { Repository } from 'typeorm'
import { CreateProductDto } from './dtos/CreateProduct.dto'
import { RequestProductDto } from './dtos/requestProduct.dto'
import { ProductParam } from './dtos/productParam.dto'
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate'
import { ProductDto } from './dtos/Product.dto'

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(Product)
        private productsRepository: Repository<Product>
    ) {}

    async createProduct(createProductDto: CreateProductDto) {
        console.log(createProductDto)
        const newProduct = this.productsRepository.create(createProductDto)
        const { userid, ...savedProduct } =
            await this.productsRepository.save(newProduct)
        return savedProduct
    }

    async getProducts({ userid, filter }: RequestProductDto) {
        const find_Products = await this.productsRepository.findBy({ userid })
        return { filter: '', products: find_Products }
    }

    async getPaginatedProducts(productParams: ProductParam) {
        const { page, limit, sort, sortvalue, userid, name } = productParams
        const options: IPaginationOptions = { page, limit }
        const sql_sort = sort == 1 ? 'DESC' : 'ASC'

        const qb = this.productsRepository
            .createQueryBuilder('product')
            .where('product.userid= :uid', { uid: userid })

        if (sortvalue) qb.addOrderBy(sortvalue, sql_sort)
        if (name) qb.andWhere('product.productname= :uname', { uname: name })

        // return qb.where('product.userid= :uid', { uid: userid })
        return paginate<ProductDto>(qb, options)
    }
}
