import { Module } from '@nestjs/common'
import { ProductsMicroServiceController } from './products.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Product } from 'src/typeorm/entities/Products'
import { ProductsService } from './products.service'
import { NatsClientModule } from 'src/nats-client/nats-client.module'

@Module({
    imports: [TypeOrmModule.forFeature([Product]), NatsClientModule],
    controllers: [ProductsMicroServiceController],
    providers: [ProductsService]
})
export class ProductsModule {}
