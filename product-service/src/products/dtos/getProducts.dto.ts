import { ProductDto } from './Product.dto'

export class GetProductDto {
    filter?: string

    products: ProductDto[]
}
