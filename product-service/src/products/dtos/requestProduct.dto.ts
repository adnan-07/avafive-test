import { ProductDto } from './Product.dto'

export class RequestProductDto {
    filter?: string

    sortvalue?: string

    sort?: string

    userid: string
}
