export class ProductParam {
    page: number

    limit: number

    name: string

    sortvalue: string

    sort: 1 | -1

    userid: string
}
