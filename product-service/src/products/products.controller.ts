import { Controller, Inject, Req, UseGuards } from '@nestjs/common'
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices'
import { CreateProductDto } from './dtos/CreateProduct.dto'
import { ProductsService } from './products.service'
import { RequestProductDto } from './dtos/requestProduct.dto'
import { ProductParam } from './dtos/productParam.dto'

@Controller('products')
export class ProductsMicroServiceController {
    constructor(
        @Inject('NATS_SERVICE') private natsClient: ClientProxy,
        private productsService: ProductsService
    ) {}

    @MessagePattern({ cmd: 'createProduct' })
    createUser(@Payload() data: CreateProductDto) {
        console.log(data)
        return this.productsService.createProduct(data)
    }

    @MessagePattern({ cmd: 'getProducts' })
    getProucts(@Payload() data: ProductParam) {
        return this.productsService.getPaginatedProducts(data)
    }
}
