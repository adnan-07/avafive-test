import { Controller, UseFilters, UseGuards } from '@nestjs/common'
import { MessagePattern, Payload } from '@nestjs/microservices'
import { CreateUserDto } from './dtos/CreateUser.dto'
import { UsersService } from './users.service'
import { AuthPayloadDto } from './dtos/auth.dto'
import { RegisterUserDto } from './dtos/registerUser.dto'

@Controller()
export class UsersMicroServiceController {
    constructor(private usersService: UsersService) {}

    @MessagePattern({ cmd: 'createUser' })
    createUser(@Payload() data: RegisterUserDto) {
        // console.log(data)

        // Note: Any data returned from here will be send to client
        // We can send custom responsed here e.g
        // { msg: 'success' }
        // return data

        return this.usersService.createUser(data)
    }

    @MessagePattern({ cmd: 'validateUser' })
    validateUser(@Payload() data: AuthPayloadDto) {
        return this.usersService.validateUser(data)
    }

    @MessagePattern({ cmd: 'getAllUsers' })
    getAllUsers() {
        return this.usersService.getAllUsers()
    }
}
