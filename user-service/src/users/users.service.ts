import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from 'src/typeorm/entities/Users'
import { Repository } from 'typeorm'
import { CreateUserDto } from './dtos/CreateUser.dto'
import { UserDto } from './dtos/User.dto'
import { AuthPayloadDto } from './dtos/auth.dto'
import { RegisterUserDto } from './dtos/registerUser.dto'
import { comparePasswords, encodePassword } from 'src/utils/bCrypt'

const fakeUsers: UserDto[] = [
    {
        email: 'abc@def.com',
        password: 'abc123!A',
        userid: 'some_id',
        status: 'local'
    },
    {
        email: 'adnan@abc.com',
        password: 'asdwdQ!121',
        userid: 'some_id',
        status: 'admin'
    }
]

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private usersRepository: Repository<User>
    ) {}

    async createUser(registerUserDto: RegisterUserDto) {
        const findUser = await this.usersRepository.findOneBy({
            email: registerUserDto.email
        })

        if (findUser) return null

        const password = encodePassword(registerUserDto.password)
        const newUser = this.usersRepository.create({
            ...registerUserDto,
            password
        })
        const createUser = await this.usersRepository.save(newUser)
        return { message: 'user created successfully' }
    }

    async validateUser({ email, password }: AuthPayloadDto) {
        // const findUser = fakeUsers.find((user) => user.email === email)
        const findUser = await this.usersRepository.findOneBy({ email })
        if (!findUser) return null

        if (comparePasswords(password, findUser.password)) {
            const { password, ...user } = findUser
            return user
        } else return null
    }

    async getAllUsers() {
        const findUsers = (await this.usersRepository.find()).map(
            ({ password, ...user }) => user
        )
        return findUsers
    }
}
