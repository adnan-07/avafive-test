import { Module } from '@nestjs/common'
import { UsersModule } from './users/users.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './typeorm/entities/Users'

// Since it is demo app, I've placed credentials here
// In production, these values will come from .env file
@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'user_db_svc',
            port: 5432,
            database: 'userdb',
            entities: [User],
            synchronize: true,
            username: 'admin',
            password: 'abc123'
        }),
        UsersModule
    ],
    controllers: [],
    providers: []
})
export class AppModule {}
