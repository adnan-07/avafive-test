import { Request } from 'express'
import { UserJwt } from 'src/auth/dtos/userJwt.dto'

export interface MyRequest extends Request {
    user: UserJwt
}
