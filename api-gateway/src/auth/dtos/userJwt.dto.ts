import { UserIdDto } from './userId.dto'

export class UserJwt extends UserIdDto {
    iat: number
    exp: number
}
