import {
    IsEmail,
    IsNotEmpty,
    IsOptional,
    IsString,
    IsStrongPassword,
    MinLength
} from 'class-validator'

export class RegisterUserDto {
    @IsNotEmpty()
    @IsEmail()
    email: string

    @IsNotEmpty()
    @MinLength(8)
    @IsStrongPassword()
    password: string

    @IsOptional()
    @IsString()
    status?: string
}
