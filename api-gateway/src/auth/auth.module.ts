import { Module } from '@nestjs/common'
import { AuthController } from './auth.controller'
import { NatsClientModule } from 'src/nats-client/nats-client.module'
import { JwtModule } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport'
import { LocalStrategy } from './strategies/local.strategy'
import { AuthService } from './auth.service'
import { JwtStrategy } from './strategies/jwt.strategy'

@Module({
    imports: [
        NatsClientModule,
        PassportModule,
        JwtModule.register({
            secret: 'abc123',
            signOptions: { expiresIn: '1d' }
        })
    ],
    controllers: [AuthController],
    providers: [LocalStrategy, JwtStrategy, AuthService]
})
export class AuthModule {}
