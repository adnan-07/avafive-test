import {
    HttpException,
    HttpStatus,
    Inject,
    Injectable,
    UnauthorizedException
} from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { AuthPayloadDto } from './dtos/auth.dto'
import { lastValueFrom } from 'rxjs'
import { UserIdDto } from './dtos/userId.dto'
import { JwtService } from '@nestjs/jwt'
import { RegisterUserDto } from './dtos/registerUser.dto'

const fakeUsers = [
    {
        email: 'abc@def.com',
        password: 'abc123!A',
        userid: 'some_id'
    }
]

@Injectable()
export class AuthService {
    constructor(
        @Inject('NATS_SERVICE') private natsClient: ClientProxy,
        private jwtService: JwtService
    ) {}

    async createUser(registerUserDto: RegisterUserDto) {
        const creation_result = await lastValueFrom(
            this.natsClient.send({ cmd: 'createUser' }, registerUserDto)
        )

        if (!creation_result) {
            throw new HttpException('user already exists', HttpStatus.FORBIDDEN)
        }

        return creation_result
    }

    async validateUser(authPayload: AuthPayloadDto) {
        const user = await lastValueFrom<UserIdDto>(
            this.natsClient.send({ cmd: 'validateUser' }, authPayload)
        )
        if (!user) return null
        return this.jwtService.sign(user)
    }
}
