import {
    Body,
    Controller,
    Inject,
    Post,
    UnauthorizedException,
    UseGuards
} from '@nestjs/common'
import { AuthPayloadDto } from './dtos/auth.dto'
import { ClientProxy } from '@nestjs/microservices'
import { lastValueFrom } from 'rxjs'
import { JwtService } from '@nestjs/jwt'
import { UserIdDto } from './dtos/userId.dto'
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth.service'
import { LocalGuard } from './local.guard'
import { RegisterUserDto } from './dtos/registerUser.dto'

@Controller('auth')
export class AuthController {
    constructor(
        @Inject('NATS_SERVICE') private natsClient: ClientProxy,
        private jwtService: JwtService,
        private authService: AuthService
    ) {}

    @Post('register')
    register(@Body() registerUserDto: RegisterUserDto) {
        return this.authService.createUser(registerUserDto)
    }

    @Post('login')
    @UseGuards(LocalGuard)
    login(@Body() authPayload: AuthPayloadDto) {
        return this.authService.validateUser(authPayload)
    }
}
