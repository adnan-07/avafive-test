import { Module } from '@nestjs/common'
import { UsersModule } from './users/users.module'
import { NatsClientModule } from './nats-client/nats-client.module'
import { ProductsModule } from './products/products.module'
import { AuthModule } from './auth/auth.module'

@Module({
    imports: [UsersModule, NatsClientModule, ProductsModule, AuthModule],
    controllers: [],
    providers: []
})
export class AppModule {}
