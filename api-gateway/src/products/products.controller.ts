import {
    Body,
    Controller,
    DefaultValuePipe,
    Get,
    Inject,
    ParseIntPipe,
    Post,
    Query,
    Req,
    UseGuards
} from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { CreateProductDto } from './dtos/createProduct.dto'
import { Request } from 'express'
import { JwtGuard } from 'src/auth/jwt.guard'
import { NewProductDto } from './dtos/newProduct.dto'
import { UserJwt } from 'src/auth/dtos/userJwt.dto'
import { RequestProductDto } from './dtos/requestProduct.dto'
import { IPaginationOptions } from 'nestjs-typeorm-paginate'
import { ProductParam } from './dtos/productParam.dto'

interface MyRequest extends Express.Request {
    user: UserJwt
}

@Controller('products')
export class ProductsController {
    constructor(@Inject('NATS_SERVICE') private natsClient: ClientProxy) {}

    @Post()
    @UseGuards(JwtGuard)
    createProduct(@Req() req: MyRequest, @Body() newProductDto: NewProductDto) {
        const { userid } = req.user
        const createProductDto: CreateProductDto = { userid, ...newProductDto }
        return this.natsClient.send({ cmd: 'createProduct' }, createProductDto)
    }

    @Get()
    @UseGuards(JwtGuard)
    getProducts(
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
        @Query('limit', new DefaultValuePipe(10), ParseIntPipe)
        limit: number = 1,
        @Query('name') name: string,
        @Query('sort', new DefaultValuePipe(1), ParseIntPipe) sort: number = 1,
        @Query('sortvalue', new DefaultValuePipe('productname'))
        sortvalue: string = 'productname',
        @Req() req: MyRequest
    ) {
        const newsort = sort >= 1 ? 1 : -1
        const newsortvalue = !['productname, price'].includes(sortvalue)
            ? 'productname'
            : sortvalue
        const { userid } = req.user
        const options: ProductParam = {
            page,
            limit,
            name,
            sortvalue: newsortvalue,
            sort: newsort,
            userid
        }
        return this.natsClient.send({ cmd: 'getProducts' }, options)
    }
}
