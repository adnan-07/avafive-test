import { IsString } from 'class-validator'
import { ProductDto } from './Product.dto'

export class GetProductDto {
    @IsString()
    filter?: string

    products: ProductDto[]
}
