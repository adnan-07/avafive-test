import { IsNotEmpty, IsPositive, IsString } from 'class-validator'

export class NewProductDto {
    @IsNotEmpty()
    @IsString()
    productname: string

    @IsNotEmpty()
    @IsPositive()
    price: number
}
