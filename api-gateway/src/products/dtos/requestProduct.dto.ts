import { IsNotEmpty, IsString } from 'class-validator'
import { ProductDto } from './Product.dto'

export class RequestProductDto {
    filter?: string

    sortvalue?: string

    sort: string
}
