import { IsNotEmpty, IsPositive, IsString } from 'class-validator'

export class CreateProductDto {
    @IsNotEmpty()
    @IsString()
    productname: string

    @IsNotEmpty()
    @IsPositive()
    price: number

    @IsNotEmpty()
    @IsString()
    userid: string
}
