import {
    IsEmail,
    IsNotEmpty,
    IsStrongPassword,
    MinLength
} from 'class-validator'

export class CreateUserDto {
    @IsNotEmpty()
    @IsEmail()
    email: string

    @IsNotEmpty()
    @MinLength(8)
    @IsStrongPassword()
    password: string
}
