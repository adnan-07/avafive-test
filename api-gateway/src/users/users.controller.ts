import {
    Controller,
    Get,
    Inject,
    Req,
    UnauthorizedException,
    UseGuards
} from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { LocalGuard } from 'src/auth/local.guard'
import { JwtGuard } from 'src/auth/jwt.guard'
import { MyRequest } from 'src/types/MyRequest'
import { lastValueFrom } from 'rxjs'

@Controller('users')
export class UsersController {
    constructor(@Inject('NATS_SERVICE') private natsClient: ClientProxy) {}

    @Get()
    @UseGuards(JwtGuard)
    getUser(@Req() { user }: MyRequest) {
        const { exp, iat, ...userDetails } = user
        return userDetails
    }

    @Get('all')
    @UseGuards(JwtGuard)
    getAllUsers(@Req() { user }: MyRequest) {
        const { status } = user
        if (status != 'admin')
            throw new UnauthorizedException(
                undefined,
                'You do not have previlages'
            )

        return lastValueFrom(this.natsClient.send({ cmd: 'getAllUsers' }, {}))
    }
}
