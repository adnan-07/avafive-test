# Endpoints

-   `POST` /auth/register - create a user

    -   `email` - string
    -   `password` - string
    -   `status` (optional) - role of user (default: local)
    -   > Note: In production version a different strategy is to be used to grant admin access to a user

-   `POST` /auth/login - user login

    -   body:
    -   `email` - string
    -   `password` - string
    -   returns a jwt token if successfully logged in (pass it as bearer token in header for protected routes)

-   `GET` /users - get logged in user details

    -   `Authorization: Bearer {jwtToken}` - in header
    -   returns a partial user object

-   `GET` /users/all - get list of all users if the user is authorized as admin

    -   `Authorization: Bearer {jwtToken}` - in header
    -   returns a list of partial user objects

-   `POST` /products - creates a product

    -   `Authorization: Bearer {jwtToken}` - in header
    -   body:
    -   `productname` - string
    -   `price` - number
    -   returns: created product object

-   `GET` /products - gets a paginated list of products the user created

    -   `Authorization: Bearer {jwtToken}` - in header
    -   query_params:
    -   `limit`(optional) - page limit (default 10)
    -   `page` (optional) - page number (default 1)
    -   `name` (optional) - parameter to filter product by name
    -   `sortvalue` (optional) - field to sory results by (default productname)
    -   `sort` (optional) - 1 = ASC, -1 = DESC (default 1)
        returns an paginated array of products

> To start the app ensure you have docker installed and run command `'docker-compose up --build'` in the root directory

# Notes

-   All credentials (database passwords, jwt secret, etc) are saved in the source code for ease of development and portability. In production environment, environment variables will be stored in and read from a ‘.env’ file

-   Ensure the end of lines (eols) in the ‘docker-compose.yml’ and ‘init-two-db.sh’ are ‘lf’ instead of ‘crlf’, else shell script won’t be found by the docker-init database script

-   Spec files are removed since its a demo project, in production spec files should be included to perform unit tests

-   The auth module handles user authentication
